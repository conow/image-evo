import numpy as np
import cv2
import random
import time
import bisect
import scipy.ndimage
from PIL import Image, ImageTk
import sys
from itertools import count
# someone decided it was a good idea to change tkinter import names between
# python 2 and 3, so we get to import differently depending on which version
# is being used
if sys.version_info[0] == 3:
    import tkinter as tk
    from tkinter import filedialog
else:
    import Tkinter as tk
    import tkFileDialog as filedialog


fit_mean = 0
itercount = 0

def sample_individual(fitness_list, fit_total):
    choice = fit_total * random.random()
    return bisect.bisect(fitness_list, choice)

def breed_inds(parent1, parent2, mut_rate, rec_rate):
    image1 = parent1.produce_image_chromosome(mut_rate, rec_rate)
    image2 = parent2.produce_image_chromosome(mut_rate, rec_rate)
    result = Image_Creature(image1, image2)
    return result

def breed_pop(pop, mut_rate, rec_rate, target_img, selection_strength, sex, popsize):
    image_size = target_img.shape[0]*target_img.shape[1]
    fitness_list = [max(1e-6, scipy.ndimage.sum(((individual.image - target_img)**2))/float(image_size*3)) for individual in pop]

    het_counts = [np.count_nonzero(np.int32(individual.image1)-np.int32(individual.image2))/float(image_size*3) for individual in pop]

    most_het = max(het_counts)
    least_het = min(het_counts)
    mean_het = np.mean(het_counts)

    most_fit = min(fitness_list)
    least_fit = max(fitness_list)
    mean_fit = np.mean(fitness_list)
    most_fit_index = fitness_list.index(most_fit)

    fit_mean = np.median(fitness_list)
    fit_max = min(fitness_list)
    fit_max1 = fit_max/fit_mean
    fit_total1 = sum(fitness_list)
    # Don't want our modified fitness values to blow up
    # If that happens, we'll modify the divisor accordingly
    # Also this may throw a runtime warning about overflow,
    # but the entire point of this scaling is to prevent
    # overflow in the fitness list so that warning is somewhat
    # expected and not a problem
    if 10**(-20000*(fit_max/fit_mean-1)*selection_strength) > 1e200:
        fit_mean = 100*fit_max/(100*selection_strength - 1)
    fitness_list = [10**(-20000*(fit/fit_mean-1)*selection_strength) for fit in fitness_list]
    sum_list = [0 for i in range(len(fitness_list))]
    sum_list[0] = fitness_list[0]
    for i in range(1,len(fitness_list)):
        sum_list[i] = sum_list[i-1] + fitness_list[i]

    fit_total = sum(fitness_list)
    fit_mean2 = np.median(fitness_list)
    newpop = list()
    samp_tot = 0
    breed_tot = 0

    for i in range(popsize):
        if sex:
            parent1 = pop[sample_individual(sum_list, fit_total)]
            parent2 = pop[sample_individual(sum_list, fit_total)]
        else:
            parent1 = parent2 = pop[sample_individual(sum_list, fit_total)]
        # not a huge fan of passing mutation and recombination rates around so much before
        # they're actually used. may be worth revisiting at some point
        newpop.append(breed_inds(parent1, parent2, mut_rate, rec_rate))

    return newpop, mean_fit, most_fit, least_fit, most_fit_index, mean_het, most_het, least_het

class Image_Pop:
    def __init__(self):
        self.indviduals = list()
        self.age = 0
    def setup_sim(self, popsize, target, start_img):
        if start_img.get() == 'Blank':
            blank_img = np.full(np.shape(target.image), (127, 127, 127))
        elif start_img.get() == 'Target':
            blank_img = target.image
        self.individuals = [Image_Creature(blank_img, blank_img) for i in range(int(popsize.get()))]
        self.age = 0

    def update(self, newpop):
        self.individuals = newpop
        self.age += 1

    def avg(self):
        result = np.zeros(self.individuals[0].image.shape, dtype='float_')
        popsize = float(len(self.individuals))
        for ind in self.individuals:
            result = cv2.addWeighted(result, 1, np.float_(ind.image), 1/popsize, 0)
        return np.uint8(result)

class Image_Creature:
    def __init__(self, image1, image2):
        self.image1 = np.copy(image1)
        self.image2 = np.copy(image2)
        # we want image to be the average of values for its two
        # "chromosomes" (complete additive interaction between alleles)
        # but since we're constrained to using uint8, we need to divide before
        # we add, otherwise the value will overflow and we won't actually get
        # average
        # also, // is forced integer division in both python2 and python3
        # this keeps the type of image as a uint8 rather than implicitly casting
        # it to float
        self.image = cv2.addWeighted(image1, 0.5, image2, 0.5, 0)
        self.num_cols = np.shape(self.image)[1]
        self.num_rows = np.shape(self.image)[0]

    def produce_image_chromosome(self, mut_rate, rec_rate):
        flip = random.choice((True, False))
        result = np.zeros(np.shape(self.image), dtype='uint8')

        num_cols = self.num_cols
        num_rows = self.num_rows

        mut_rate = mut_rate * num_cols
        num_mut = min(np.random.poisson(mut_rate*num_rows), num_cols*num_rows)
        mut_sites = random.sample(range(num_cols*num_rows), num_mut)

        rec_rate = rec_rate * num_rows
        num_rec = min(np.random.poisson(num_cols*rec_rate), num_rows*num_cols)
        rec_sites = sorted(random.sample(range(num_rows*num_cols), num_rec))
        previous_i = 0
        previous_j = 0
        for loc in rec_sites:
            i = loc // num_cols
            j = loc % num_cols
            result[previous_i:i] = self.image2[previous_i:i] if flip else self.image1[previous_i:i]
            if previous_i != i:
                previous_j = 0
            result[i][previous_j:j] = self.image2[i][previous_j:j] if flip else self.image1[i][previous_j:j]
            result[i][j:] = self.image1[i][j:] if flip else self.image2[i][j:]
            which_pix = random.randint(0,2)
            result[i][j][0:which_pix] = self.image2[i][j][0:which_pix] if flip else self.image1[i][j][0:which_pix]
            result[i][j][which_pix:] = self.image1[i][j][which_pix:] if flip else self.image2[i][j][which_pix:]
            previous_i = i
            previous_j = j
            flip = not flip

        result[previous_i:] = self.image2[previous_i:] if flip else self.image1[previous_i:]

        for loc in mut_sites:
            i = loc // num_cols
            j = loc % num_cols
            
            mut_contrib = 0.6
            new_channel = random.randint(0,255)
            which_channel = random.randint(0,2)
            result[i][j][which_channel] = result[i][j][which_channel]*(1-mut_contrib) + new_channel*mut_contrib
            """             
            new_red = random.randint(0,255)
            new_green = random.randint(0,255)
            new_blue = random.randint(0,255)
            result[i][j] = result[i][j]*(1-mut_contrib) + np.array([new_red, new_green, new_blue])*mut_contrib
            """
        return result



class ShownImage:
    def __init__(self):
        self.image = None
    def update(self, image):
        self.image = image

def fromCv2Image(image, maxdims = -1, maxdims2 = -1):
    height, width, channels = image.shape
    if maxdims > 0 and maxdims2 < 0:
        if height > width:
            width = int(maxdims * width / float(height))
            height = maxdims
        else:
            height = int(maxdims * height / float(width))
            width = maxdims
    elif maxdims > 0 and maxdims2 > 0:
        width = maxdims
        height = maxdims2
    return ImageTk.PhotoImage(Image.fromarray(cv2.cvtColor(image, cv2.COLOR_BGR2RGB)).resize((width,height), resample=Image.BOX))

def select_image(panel, mut_per_ind, rec_per_ind, target, start_sim_btn, reset_sim_btn, step_btn):
    if target.image is None:
        start_sim_btn.config(state=tk.DISABLED)
        #reset_sim_btn.config(state=tk.DISABLED)
        step_btn.config(state=tk.DISABLED)
    path = filedialog.askopenfilename(initialdir='.')
    if len(path) > 0:
        target.update(cv2.imread(path))
        target_height, target_width, _ = target.image.shape
        larger_dim = max(target_height, target_width)
        max_dim = 100
        scale = 1 if max_dim > larger_dim else float(max_dim) / larger_dim
        target.update(cv2.resize(target.image, dsize=(0,0), fx = scale, fy = scale))
        image = fromCv2Image(target.image, 300)
        panel.configure(image = image, height = 300, width = 300)
        panel.image = image
        # setting mut/rec_per_ind equal to itself triggers its trace which makes mut_per_pix update to the 
        # correct value for this image
        mut_per_ind.set(mut_per_ind.get())
        rec_per_ind.set(rec_per_ind.get())
        start_sim_btn.config(state=tk.NORMAL)
        step_btn.config(state=tk.NORMAL)

def set_rate_per_pop(rate_per_pop, rate_per_ind, popsize):
    rate_per_ind = float(rate_per_ind) if (rate_per_ind != ''  and rate_per_ind != '.') else 0
    if popsize.get() == '' or popsize.get() == '0':
        popsizeval = 1
    else:
        popsizeval = int(popsize.get())
    rate_per_pop.set(str(rate_per_ind*popsizeval))

def set_rate_per_pix(rate_per_pix, rate_per_ind, image):
    imsize = image.image.shape[0]*image.image.shape[1] if image.image is not None else 1
    rate_per_ind = float(rate_per_ind) if (rate_per_ind != '' and rate_per_ind != '.') else 0
    rate_per_pix.set(str(rate_per_ind/imsize))

def update_pop(popsize, mut_per_ind, mut_per_pop, rec_per_ind, rec_per_pop):
    set_rate_per_pop(mut_per_pop, mut_per_ind.get(), popsize)
    set_rate_per_pop(rec_per_pop, rec_per_ind.get(), popsize)

def check_rate_per_ind(val, rate_per_pop, rate_per_pix, popsize, image, updating_entry):
    if updating_entry.get():
        return False
    if val == '':
        val = '0'
    updating_entry.set(True)
    set_rate_per_pop(rate_per_pop, val, popsize)
    set_rate_per_pix(rate_per_pix, val, image)
    updating_entry.set(False)
    return True

def check_rate_per_pop(val, rate_per_ind, rate_per_pix, popsize, image, updating_entry):
    if updating_entry.get():
        return False
    updating_entry.set(True)
    if val == '':
        val = '0'
    if popsize.get() == '' or popsize.get() == '0':
        popsizeval = 1
    else:
        popsizeval = int(popsize.get())
    rate_per_ind.set(str(float(val)/popsizeval))
    set_rate_per_pix(rate_per_pix, rate_per_ind.get(), image)
    updating_entry.set(False)
    return True

def check_rate_per_pix(val, rate_per_ind, rate_per_pop, popsize, image, updating_entry):
    if updating_entry.get():
        return False
    if val == '':
        val = '0'
    updating_entry.set(True)
    imsize = image.image.shape[0]*image.image.shape[1] if image.image is not None else 1
    rate_per_ind.set(str(float(val)*imsize))
    set_rate_per_pop(rate_per_pop, rate_per_ind.get(), popsize)
    updating_entry.set(False)
    return True

def start_sim(running, start_sim_btn, select_target_btn, other_UI_stuff):
    running.set(True)
    start_sim_btn.config(text='Pause simulation', command=lambda:pause_sim(running, start_sim_btn, select_target_btn, other_UI_stuff))
    select_target_btn.config(state=tk.DISABLED)
    for element in other_UI_stuff:
        element.config(state=tk.DISABLED)

def pause_sim(running, start_sim_btn, select_target_btn, other_UI_stuff):
    running.set(False)
    start_sim_btn.config(text='Start simulation', command=lambda:start_sim(running, start_sim_btn, select_target_btn, other_UI_stuff))
    for element in other_UI_stuff:
        element.config(state=tk.NORMAL)

def reset_sim(started, select_target_btn, reset_sim_btn):
    started.set(False)
    #reset_sim_btn.config(state=tk.DISABLED)
    select_target_btn.config(state=tk.NORMAL)


# In a perfect world, this would optimally pack individual images into the population panel based on their 
# dimensions, but I just need this to work so we're going to smash the images into a shape that packs effectively
# in the panel. Causes some distortion, but the population is still readily observable
def produce_pop_panel(pop, pop_panel):
    panel_width = 900
    panel_height = 300
    panel_ratio = panel_width/float(panel_height)
    panel_pix = panel_width*panel_height
    pix_per_square = panel_pix / float(len(pop))
    square_side = int(np.sqrt(pix_per_square))
    num_rows = panel_height // (square_side)
    num_cols = panel_width // (square_side)
    diff = len(pop) - num_rows * num_cols
    if diff != 0:
        if diff < num_rows:
            num_cols = num_cols + 1
        else:
            num_rows = num_rows + 1        
    panel_img = np.zeros((num_rows*square_side, num_cols*square_side, 3), dtype='uint8')
    count = 0
    blank = np.full(pop[0].image.shape, (127,127,127), dtype='uint8')
    blank_ind = Image_Creature(blank,blank)
    for row in range(num_rows):
        row_start = row*square_side
        row_end = min(count+num_cols, len(pop))
        cv2.resize(blank_ind.image,(square_side, square_side))
        panel_img[row_start:row_start+square_side] = np.concatenate([cv2.resize(ind.image, (square_side, square_side), interpolation=cv2.INTER_AREA) for ind in pop[count:row_end]+[blank_ind for i in range(count+num_cols-row_end)]], axis=1)
        count = count + num_cols
    panel_img = fromCv2Image(panel_img, panel_width, panel_height)
    pop_panel.configure(image=panel_img, height=panel_height, width=panel_width)
    pop_panel.image = panel_img
    
    
def simulation_loop(running, started, mut_per_pix, rec_per_pix, popsize, selection_strength, target, iterval, pop, master, fitst_panel, mean_panel, pop_panel, start_img, mean_fitness, best_fit, worst_fit, mean_het, most_het, least_het, generations, sex, step):
    if iterval.get() != '' and int(iterval.get()) == 0:
        running.set(False)
    if not running.get() and not step.get():
        # if the simulation isn't supposed to be running, wait a little bit so that we don't max out the CPU
        # doing nothing
        master.after(10, simulation_loop, running, started, mut_per_pix, rec_per_pix, popsize, selection_strength, target, iterval, pop, master, first_panel, mean_panel, pop_panel, start_img, mean_fitness, best_fit, worst_fit, mean_het, most_het, least_het, generations, sex, step)
    else:
        step.set(False)
        popsizeval = int(popsize.get()) if (popsize.get() != '' and popsize.get() != '0') else 1
        if not started.get():
            pop.setup_sim(popsize, target, start_img)
            started.set(True)
        ss = float(selection_strength.get()) if (selection_strength.get() != '' and selection_strength.get() != '.') else 0.
        newpop, mean_fit, most_fit, least_fit, most_fit_index, mean_h, most_h, least_h = breed_pop(pop.individuals, float(mut_per_pix.get()), float(rec_per_pix.get()), target.image.astype('int'), ss, sex.get(), popsizeval)

        if pop.age > 0: 
            mean_image = fromCv2Image(pop.avg(),300)
            mean_panel.configure(image=mean_image, height=300, width=300)
            mean_panel.image = mean_image
            first_image = fromCv2Image(pop.individuals[most_fit_index].image, 300)
            first_panel.configure(image=first_image, height=300, width=300)
            first_panel.image = first_image
            produce_pop_panel(pop.individuals, pop_panel)
        pop.update(newpop)
        if iterval.get() != '':
            iterval.set(str(max(int(iterval.get())-1, 0)))
        mean_fitness.set(str(mean_fit))
        best_fit.set(str(most_fit))
        worst_fit.set(str(least_fit))
        mean_het.set(str(mean_h))
        most_het.set(str(most_h))
        least_het.set(str(least_h))
        generations.set(str(pop.age))
        master.after(16, simulation_loop, running, started, mut_per_pix, rec_per_pix, popsize, selection_strength, target, iterval, pop, master, first_panel, mean_panel, pop_panel, start_img, mean_fitness, best_fit, worst_fit, mean_het, most_het, least_het, generations, sex, step)

if __name__ == '__main__':
    root = tk.Tk()
    other_UI_stuff = list()
    check_int = root.register(lambda a: str.isdigit(a) or a == '')
    check_decimal = root.register(lambda a: all([str.isdigit(i) or i == '' for i in a.split('.')]) and len(a.split('.')) <= 2)
    check_decimal_less_1 = root.register(lambda a: all([str.isdigit(i) or i == '' for i in a.split('.')]) and len(a.split('.')) <= 2 and (a == '' or a == '.' or float(a) <= 1))

    popsize = tk.StringVar()
    popsize.set('20')

    target = ShownImage()
    
    updating_entry = tk.BooleanVar()

    mut_per_ind = tk.StringVar()
    mut_per_ind.set('1')

    mut_per_pop = tk.StringVar()
    set_rate_per_pop(mut_per_pop, mut_per_ind.get(), popsize)

    mut_per_pix = tk.StringVar()
    set_rate_per_pix(mut_per_pix, mut_per_ind.get(), target)

    rec_per_ind = tk.StringVar()
    rec_per_ind.set('10')

    rec_per_pop = tk.StringVar()
    set_rate_per_pop(rec_per_pop, rec_per_ind.get(), popsize)

    rec_per_pix = tk.StringVar()
    set_rate_per_pix(rec_per_pix, rec_per_ind.get(), target)

    selection_strength = tk.StringVar()
    selection_strength.set('1')

    iterval = tk.StringVar()
    iterval.set('')
    
    mean_fitness = tk.StringVar()
    best_fit = tk.StringVar()
    worst_fit = tk.StringVar()

    mean_het = tk.StringVar()
    most_het = tk.StringVar()
    least_het = tk.StringVar()

    sex = tk.BooleanVar()
    sex.set(True)
    
    generations = tk.StringVar()
    generations.set('0')

    mut_per_ind.trace("w", lambda *_: check_rate_per_ind(mut_per_ind.get(), mut_per_pop, mut_per_pix, popsize, target, updating_entry))
    mut_per_pop.trace("w", lambda *_: check_rate_per_pop(mut_per_pop.get(), mut_per_ind, mut_per_pix, popsize, target, updating_entry))
    mut_per_pix.trace("w", lambda *_: check_rate_per_pix(mut_per_pix.get(), mut_per_ind, mut_per_pop, popsize, target, updating_entry))

    rec_per_ind.trace("w", lambda *_: check_rate_per_ind(rec_per_ind.get(), rec_per_pop, rec_per_pix, popsize, target, updating_entry))
    rec_per_pop.trace("w", lambda *_: check_rate_per_pop(rec_per_pop.get(), rec_per_ind, rec_per_pix, popsize, target, updating_entry))
    rec_per_pix.trace("w", lambda *_: check_rate_per_pix(rec_per_pix.get(), rec_per_ind, rec_per_pop, popsize, target, updating_entry))

    target_panel_label = tk.Label(root, text='Target')
    target_panel_label.grid(column=3, row=0)
    target_panel = tk.Label(root)
    target_panel.grid(column=3,row=1,rowspan=10)

    select_target_btn = tk.Button(root, text='Select image', command=lambda:select_image(target_panel, mut_per_ind, rec_per_ind, target, start_sim_btn, reset_sim_btn, step_btn))
    select_target_btn.grid(row=0,column=0, padx=5, pady=5)

    popsize.trace("w", callback=lambda *_: update_pop(popsize, mut_per_ind, mut_per_pop, rec_per_ind, rec_per_pop))


    running = tk.BooleanVar(False)
    started = tk.BooleanVar(False)

    prev_row = count(start=1, step=0.5)
    # increments by 1 every 2 calls. This lets us more easily add/remove entry-label pairs without needing to 
    # manually change the row numbers of everything after it
    next_row = lambda : int(next(prev_row))

    pop_entry = tk.Entry(root, validate='key', validatecommand=(check_int, '%P'), textvariable=popsize)
    pop_entry.grid(column=1,row=next_row(), padx=5, pady=5)
    pop_label = tk.Label(root, text='Population size', justify='left')
    pop_label.grid(column=0,row=next_row(), sticky=tk.W)
    other_UI_stuff.append(pop_entry)
    
    iter_entry = tk.Entry(root, validate='key',validatecommand=(check_int, '%P'), textvariable=iterval)
    iter_entry.grid(column=1,row=next_row(), padx=5, pady=5)
    iter_label = tk.Label(root, text='Generations remaining', justify='left')
    iter_label.grid(column=0,row=next_row(), sticky=tk.W)
    other_UI_stuff.append(iter_entry)

    step = tk.BooleanVar()
    step.set(False)
    step_btn = tk.Button(root, text='Step', command=lambda: step.set(True), state=tk.DISABLED)
    step_btn.grid(column=2, row = iter_entry.grid_info()['row'], sticky=tk.W, padx=5)
    other_UI_stuff.append(step_btn)

    mut_per_label = tk.Label(root, text='Average mutations per', justify='left')
    mut_per_label.grid(column=0, row=next_row(), columnspan=2, sticky=tk.W)
    # since this label is the only thing on this row for these two columns,
    # we need to call next_row an extra time
    next_row()
    
    mut_per_ind_label = tk.Label(root, text='    individual', justify='left')
    mut_per_ind_label.grid(column=0, row=next_row(), sticky=tk.W)
    mut_per_ind_entry = tk.Entry(root,validate='key',validatecommand=(check_decimal,'%P'),textvariable=mut_per_ind)
    mut_per_ind_entry.grid(column=1, row=next_row(), padx=5, pady=5)
    other_UI_stuff.append(mut_per_ind_entry)

    mut_per_pop_label = tk.Label(root, text='    population', justify='left')
    mut_per_pop_label.grid(column=0, row=next_row(), sticky=tk.W)
    mut_per_pop_entry = tk.Entry(root,validate='key',validatecommand=(check_decimal,'%P'),textvariable=mut_per_pop)
    mut_per_pop_entry.grid(column=1, row=next_row(), padx=5, pady=5)
    other_UI_stuff.append(mut_per_pop_entry)

    mut_per_pix_label = tk.Label(root, text='    pixel', justify='left')
    mut_per_pix_label.grid(column=0, row=next_row(), sticky=tk.W)
    mut_per_pix_entry = tk.Entry(root,validate='key',validatecommand=(check_decimal, '%P'),textvariable=mut_per_pix)
    mut_per_pix_entry.grid(column=1, row=next_row(), padx=5, pady=5)
    other_UI_stuff.append(mut_per_pix_entry)

    rec_per_label = tk.Label(root, text='Average recombination per')
    rec_per_label.grid(column=0, row=next_row(), columnspan=2, sticky=tk.W)
    next_row()
    
    rec_per_ind_label = tk.Label(root, text='    individual')
    rec_per_ind_label.grid(column=0, row=next_row(), sticky=tk.W)
    rec_per_ind_entry = tk.Entry(root,validate='key',validatecommand=(check_decimal,'%P'),textvariable=rec_per_ind)
    rec_per_ind_entry.grid(column=1, row=next_row(), padx=5, pady=5)
    other_UI_stuff.append(rec_per_ind_entry)

    rec_per_pop_label = tk.Label(root, text='    population')
    rec_per_pop_label.grid(column=0, row=next_row(), sticky=tk.W)
    rec_per_pop_entry = tk.Entry(root,validate='key',validatecommand=(check_decimal,'%P'),textvariable=rec_per_pop)
    rec_per_pop_entry.grid(column=1, row=next_row(), padx=5, pady=5)
    other_UI_stuff.append(rec_per_pop_entry)

    rec_per_pix_label = tk.Label(root, text='    pixel')
    rec_per_pix_label.grid(column=0, row=next_row(), sticky=tk.W)
    rec_per_pix_entry = tk.Entry(root,validate='key',validatecommand=(check_decimal,'%P'),textvariable=rec_per_pix)
    rec_per_pix_entry.grid(column=1, row=next_row(), padx=5, pady=5)
    other_UI_stuff.append(rec_per_pix_entry)

    selection_strength_label = tk.Label(root, text='Selection strength')
    selection_strength_label.grid(column=0, row=next_row(), sticky=tk.W)
    selection_strength_entry = tk.Entry(root, validate='key', validatecommand=(check_decimal_less_1, '%P'), textvariable=selection_strength)
    selection_strength_entry.grid(column=1, row=next_row(), padx=5, pady=5)
    other_UI_stuff.append(selection_strength_entry)

    start_img_label = tk.Label(root, text='Starting image')
    start_img_label.grid(column=0, row=next_row(), sticky=tk.W)
    start_img = tk.StringVar()
    start_img.set('Blank')
    start_img_menu = tk.OptionMenu(root, start_img, 'Blank', 'Target')
    start_img_menu.grid(column=1, row=next_row(), padx=5, pady=10, sticky=tk.W)
    other_UI_stuff.append(start_img_menu)

    sex_check_label = tk.Label(root, text='Sexual reproduction')
    sex_check_label.grid(column=0, row=next_row(), sticky=tk.W)
    sex_check = tk.Checkbutton(root, variable=sex, onvalue=True, offvalue=False)
    sex_check.grid(column=1, row=next_row(), stick=tk.W)

    pop_stats_label = tk.Label(root, text='Population Statistics')
    pop_stats_label.grid(column = 0, row = next_row(), columnspan=2)
    next_row()
   
    generations_label = tk.Label(root, text='Generations')
    generations_label.grid(column=0, row=next_row(), sticky=tk.W)
    generations_val = tk.Entry(root, textvariable=generations, state=tk.DISABLED)
    generations_val.grid(column=1, row=next_row(), sticky=tk.W, padx=5, pady=5)

    mean_fit_label = tk.Label(root, text='Mean fitness:')
    mean_fit_label.grid(column=0, row=next_row(), sticky=tk.W)
    mean_fit_val = tk.Entry(root, textvariable=mean_fitness,state=tk.DISABLED)
    mean_fit_val.grid(column=1, row=next_row(), sticky=tk.W, padx=5, pady=5)

    best_fit_label = tk.Label(root, text='Best fitness:')
    best_fit_label.grid(column=0, row=next_row(), sticky=tk.W)
    best_fit_val = tk.Entry(root, textvariable=best_fit, state=tk.DISABLED)
    best_fit_val.grid(column=1, row=next_row(), sticky=tk.W, padx=5, pady=5)

    worst_fit_label = tk.Label(root, text='Worst fitness:')
    worst_fit_label.grid(column=0, row=next_row(), sticky=tk.W)
    worst_fit_val = tk.Entry(root, textvariable=worst_fit, state=tk.DISABLED)
    worst_fit_val.grid(column=1, row=next_row(), sticky=tk.W, padx=5, pady=5)

    mean_het_label = tk.Label(root, text='Mean heterozygosity:')
    mean_het_label.grid(column=0, row=next_row(), sticky=tk.W)
    mean_het_val = tk.Entry(root, textvariable=mean_het,state=tk.DISABLED)
    mean_het_val.grid(column=1, row=next_row(), sticky=tk.W, padx=5, pady=5)


    most_het_label = tk.Label(root, text='Most heterozygosity:')
    most_het_label.grid(column=0, row=next_row(), sticky=tk.W)
    most_het_val = tk.Entry(root, textvariable=most_het, state=tk.DISABLED)
    most_het_val.grid(column=1, row=next_row(), sticky=tk.W, padx=5, pady=5)

    least_het_label = tk.Label(root, text='Least heterozygosity:')
    least_het_label.grid(column=0, row=next_row(), sticky=tk.W)
    least_het_val = tk.Entry(root, textvariable=least_het, state=tk.DISABLED)
    least_het_val.grid(column=1, row=next_row(), sticky=tk.W, padx=5, pady=5)
    
    start_sim_btn = tk.Button(root, text='Start simulation', command=lambda:start_sim(running, start_sim_btn, select_target_btn, other_UI_stuff), state=tk.DISABLED)
    start_sim_btn.grid(row=0,column=1, padx=5, pady=5)
        
    reset_sim_btn = tk.Button(root, text='Reset simulation', command=lambda:reset_sim(started, select_target_btn, reset_sim_btn))
    reset_sim_btn.grid(row=0, column=2, padx=5, pady=5)
    other_UI_stuff.append(reset_sim_btn)

    pop = Image_Pop()
    
    first_panel_label = tk.Label(root, text='Fittest')
    first_panel_label.grid(column=4,row=0)
    first_panel = tk.Label(root)
    first_panel.grid(column=4,row=1,rowspan=10)
    
    mean_panel_label = tk.Label(root, text='Average')
    mean_panel_label.grid(column=5, row=0)
    mean_panel = tk.Label(root)
    mean_panel.grid(column=5, row=1, rowspan=10)

    pop_panel_label = tk.Label(root, text='Population')
    pop_panel_label.grid(column=3, row=11, sticky=tk.W)
    pop_panel = tk.Label(root)
    pop_panel.grid(column=3,row=12,columnspan=3,rowspan=100)
    
    root.after(10, simulation_loop, running, started, mut_per_pix, rec_per_pix, popsize, selection_strength, target, iterval, pop, root, first_panel, mean_panel, pop_panel, start_img, mean_fitness, best_fit, worst_fit, mean_het, most_het, least_het, generations, sex, step)
    root.mainloop()

