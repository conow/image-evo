# README #

This is at least initially intended to be an introduction to evolutionary algorithms and a visual demonstration of various evolutionary forces in action. Written by Chris Conow for Bisc 313 at USC. It is a fun computational "toy" that can be used to observe how things like mutation rates, population sizes, strength of selection, and sexual or asexual reproduction affect the process of evolution.

### Dependencies ###

All of the dependencies are available through `pip`. Simply run `pip install` followed by the package names.

`numpy`

`opencv-python`

`pillow`


### Running ###

![](https://bitbucket.org/conow/image-evo/raw/ab22dc695c946120852f22093bf635862e162d23/docs/images/Annotation%202020-07-16%20161925.png)

*Initial view when the program starts*

Once the dependencies are installed, simply run `image-evo.py` and then choose "Select image" in the upper left to choose an image to use as a target. Included in this repository is a small pixel art image of a cat face, but feel free to choose any image you would like. Large images will be scaled down to improve the speed of the program.

#### Settings ####

![](https://bitbucket.org/conow/image-evo/raw/ab22dc695c946120852f22093bf635862e162d23/docs/images/Annotation%202020-07-16%20161956.png)

*Sample image loaded, and simulation started*

Population size: the number of distinct individuals in the population at any given iteration

Generations remaining: the number of generations (iterations) to run the simulation. Leaving this blank lets the simulation run indefinitely.

Average mutations: mutations are chosen via a poisson process per individual. The number of expected mutations can be set per individual, population, or pixel. Changing one value automatically updates the other values.

Average recombination: same as mutations, except recombination events (i.e. crossing over). Individuals possess two copies of the image that is being evolved, and the image that is displayed is the mean value per pixel of those two "genetic" images. When reproduction occurs, new "genetic" images are created by taking subsets of pixels from each of the two "genetic" images. Recombination "flips" which image is donating "genetic material" to the offspring image.

Selection strength: floating point number in the range from 0 to 1, where 0 is no selection, and 1 is the strongest selection that the simulation allows. Stronger selection will more harshly penalize "bad" mutations (i.e. mutations that cause an individual to be more different from the target image than others in the population), while weaker selection will be more permissive of such mutations.

Starting image: What the population is initialized to. The options are blank (default) and target. When blank is chosen, every pixel is initialized to a neutral grey (128, 128, 128 in RGB), while when target is chosen, the population is initialized to already be exactly the target image. The latter case is useful to observe how genetic drift can prevent a "perfect" phenotype from being maintained under various conditions and to various degrees.

Sexual reproduction: If checked, new individuals are born by crossing two members of the previous population. If unchecked, new individuals are born by single members of the previous population essentially mating with themselves.

### Population statistics ###

Generations: the number of generations (iterations) that have elapsed.

Mean fitness: mean population fitness (fitness is measured as the mean difference in pixel values between an individual and the target image. Lower is better).

Best fitness: the best (lowest value) fitness in the population for the current generation.

Worst fitness: the worst (highest value) fitness in the population for the current generation.

Mean heterozygosity: the mean number of pixels that are different between the two "genetic" images that each individual has that are combined to produce the image that is displayed for an individual.

Most heterozygosity: the highest degree of heterozygosity of any individual in the population for the current generation.

Least heterozygosity: the lowest degree of heterozygosity of any individual in the population for the current generation.

### Visual output ###

![](https://bitbucket.org/conow/image-evo/raw/0628a7983ebacc9a3729c13d2e57161ef0a9274e/docs/images/Annotation%202020-07-16%20162516.png)

*Results after letting the simulation evolve toward the sample image for a few minutes*

Target: the target image.

Fittest: the image produced by the fittest individual for the current generation.

Average: the image produced when the entire population is averaged together.

Population: a grid showing scaled down versions of all the individuals in the population for the current generation.